package application;
//package application;

import java.net.URL;
import java.util.ResourceBundle;
//import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
//import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Controler implements Initializable{
	/*
	 * schema Controler-ului
	 * */

    @FXML
    private Button adunareButon;

    @FXML
    private Button scadereButon;

    @FXML
    private Button integrareButon;

    @FXML
    private TextField primulPolinomTextField;

    @FXML
    private Button inmultireButon;

    @FXML
    private Button impartireButon;

    @FXML
    private TextField rezultat;

    @FXML
    private TextField alDoileaPolinomTextField;

    @FXML
    private Button derivareButon;
    
    private String stringPrimulPolinom = new String();
    private String stringAlDoileaPolinom = new String();
    private String stringRezultat = new String();
    private Polinom primulPolinom = new Polinom();
    private Polinom alDoileaPolinom = new Polinom();
    private Polinom rezultatPolinom = new Polinom();
   
    /*
     * 
     * Capteaza intr-un string textul introdus in primul textField (primulPolinomTextField)
     * trimite stringul la Metoda 'regexPolinome' care returneaza un obiect Polinom cu elementele Monom 
     */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		primulPolinomTextField.setOnKeyReleased(new EventHandler <Event>(){
			@Override
			public void handle(Event primulPolinomEvent) {
				
				stringPrimulPolinom = primulPolinomTextField.getText();
				primulPolinom = Operatii.regexPolinoame(stringPrimulPolinom);
				
			
			}
			});
		/*
	     * 
	     * Capteaza intr-un string textul introdus in primul textField (primulPolinomTextField)
	     * trimite stringul la Metoda 'regexPolinome' care returneaza un obiect Polinom cu elementele Monom 
	     */
		alDoileaPolinomTextField.setOnKeyReleased(new EventHandler <Event>(){
			@Override
			public void handle(Event alDoileaPolinomEvent) {
				
				stringAlDoileaPolinom = alDoileaPolinomTextField.getText();
				alDoileaPolinom = Operatii.regexPolinoame(stringAlDoileaPolinom);
				
				
			}
			});
		/*
		 * La apasarea butonului de adunare se apealeaza functia adunare
		 * -se converteste obietul Polinom rezultat la un string
		 * -stringul este afisat in zona de text 'Rezultat'
		 * Se apeleaza din nou functia regex pentru a retine polinoamele din primele 2 zone de text
		 * pentru a nu schimba valoarea polinoamelor cu care s-a lucrat
	     */
		adunareButon.setOnAction(new EventHandler<ActionEvent>() {
		
			@Override
			public void handle(ActionEvent adunare) {
				
				
				rezultatPolinom = Operatii.adunare(primulPolinom, alDoileaPolinom);				

				rezultat.setText(rezultatPolinom.stringPolinom());
				primulPolinom = Operatii.regexPolinoame(stringPrimulPolinom);
				alDoileaPolinom = Operatii.regexPolinoame(stringAlDoileaPolinom);
				
			}
			
		});
		/*
		 * La apasarea butonului de scadere se apealeaza functia scadere
		 * -se converteste obietul Polinom rezultat la un string
		 * -stringul este afisat in zona de text 'Rezultat'
		 * Se apeleaza din nou functia regex pentru a retine polinoamele din primele 2 zone de text
		 * pentru a nu schimba valoarea polinoamelor cu care s-a lucrat
	     */
		scadereButon.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent scadere) {
				
				rezultatPolinom = Operatii.scadere(primulPolinom, alDoileaPolinom);
				rezultat.setText(rezultatPolinom.stringPolinom());
				primulPolinom = Operatii.regexPolinoame(stringPrimulPolinom);
				alDoileaPolinom = Operatii.regexPolinoame(stringAlDoileaPolinom);
			}
			
		});
		/*
		 * La apasarea butonului de inmultire se apealeaza functia inmultire
		 * -se converteste obietul Polinom rezultat la un string
		 * -stringul este afisat in zona de text 'Rezultat'
		 * Se apeleaza din nou functia regex pentru a retine polinoamele din primele 2 zone de text
		 * pentru a nu schimba valoarea polinoamelor cu care s-a lucrat
	     */
	inmultireButon.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent inmultire) {
				
				rezultatPolinom = Operatii.inmultire(primulPolinom, alDoileaPolinom);

				rezultat.setText(rezultatPolinom.stringPolinom());
				primulPolinom = Operatii.regexPolinoame(stringPrimulPolinom);
				alDoileaPolinom = Operatii.regexPolinoame(stringAlDoileaPolinom);
			}
			
		});
	/*
	 * La apasarea butonului de impartie se apealeaza functia impartire
	 * -se converteste obietul Polinom rezultat la un string
	 * -stringul este afisat in zona de text 'Rezultat'
	 * Se apeleaza din nou functia regex pentru a retine polinoamele din primele 2 zone de text
	 * pentru a nu schimba valoarea polinoamelor cu care s-a lucrat
     */
	impartireButon.setOnAction(new EventHandler<ActionEvent>() {
		
		@Override
		public void handle(ActionEvent impartire) {
			

			stringRezultat = Operatii.impartire(primulPolinom, alDoileaPolinom);
			rezultat.setText(stringRezultat);
			primulPolinom = Operatii.regexPolinoame(stringPrimulPolinom);
			alDoileaPolinom = Operatii.regexPolinoame(stringAlDoileaPolinom);
		}
		
	});
	/*
	 * La apasarea butonului de integrare se apealeaza functia integrare
	 * -se converteste obietul Polinom rezultat la un string
	 * -stringul este afisat in zona de text 'Rezultat'
	 * Se apeleaza din nou functia regex pentru a retine polinoamele din primele 2 zone de text
	 * pentru a nu schimba valoarea polinoamelor cu care s-a lucrat
     */
	integrareButon.setOnAction(new EventHandler<ActionEvent>() {
		
		@Override
		public void handle(ActionEvent integrare) {
			
			rezultatPolinom = Operatii.integrare(primulPolinom);

			rezultat.setText(rezultatPolinom.stringPolinom());
			primulPolinom = Operatii.regexPolinoame(stringPrimulPolinom);
			alDoileaPolinom = Operatii.regexPolinoame(stringAlDoileaPolinom);
		}
		
	});
	/*
	 * La apasarea butonului de derivare se apealeaza functia derivare
	 * -se converteste obietul Polinom rezultat la un string
	 * -stringul este afisat in zona de text 'Rezultat'
	 * Se apeleaza din nou functia regex pentru a retine polinoamele din primele 2 zone de text
	 * pentru a nu schimba valoarea polinoamelor cu care s-a lucrat
     */
	derivareButon.setOnAction(new EventHandler<ActionEvent>() {
		
		@Override
		public void handle(ActionEvent derivare) {
			
			rezultatPolinom = Operatii.derivare(primulPolinom);

			rezultat.setText(rezultatPolinom.stringPolinom());
			primulPolinom = Operatii.regexPolinoame(stringPrimulPolinom);
			alDoileaPolinom = Operatii.regexPolinoame(stringAlDoileaPolinom);
		}
		
	});
	}
}