package application;
//package application;

import java.text.DecimalFormat;
/*	Clasa ce defineste un Monom cu mai multe functii
 * Colectia de obiecte de tip Monom este retinuta de clasa Polinom intr-o lista de obiecte
 * 
 */
public class Monom  {
	private double coef;
	private int putere;		  
	/*
	 * Formatul pentru afisarea unui numar Double,pentru afisarea coeficientilor 
	 */
	private DecimalFormat numberFormat = new DecimalFormat("#0.##");
	
	/*
	 * Constructorul cu paramestri al clasei Monom
	 */
	public Monom(int coef, int putere){
		this.coef = coef;
		this.putere = putere;
	}
	/*
	 * Constructorul fara paramestri al clase Monom
	 */
	public Monom(){
		
	}
	/*
	 * seteaza coeficientul
	 */
	public void setCoef(double coef){
		this.coef=coef;
		
	}
	/*
	 * seteaza puterea
	 */
	public void setPutere(int putere){
		this.putere=putere;
		
	}
	/*
	 * returneaza coeficientul
	 */
	public double getCoef(){
		return this.coef;
	}
	/*
	 * returneaza puterea
	 */
	public int getPutere(){
		return this.putere;
	}
	/*Converteste un coeficient la tipul String
	 * 
	 */
	public String coefString(){
		String stringCoef = new String();
		stringCoef=numberFormat.format(this.coef);
		return stringCoef;
	}
	/*
	 * Returneaza un strinc continand valoarea puterii Monomului
	 */
	public String putereString(){
		String stringPutere = new String();
		if(this.putere!=0){

			stringPutere =""+this.putere;
		}else{
			stringPutere ="";
		}		
		return stringPutere;
	}
	
}
