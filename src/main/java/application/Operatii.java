package application;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Operatii {
/*
* Functia sorteaza elementele in ordine descrescatoare a puterilor folosind metoda "sortarePuteri",
* scoate coeficienti cu valoarea '0' folosint functia "scoateCoeficientiZero"
*/
	public static ArrayList<Monom> sortare(ArrayList<Monom> polinomNesortat) {
		ArrayList<Monom> polinomSortat = new ArrayList<Monom>();
		int marime = polinomNesortat.size();
		for (int i = 0; i < marime; i++) {
			for (int z = 0; z < polinomSortat.size(); z++) {
				if (polinomSortat.get(z).getPutere() == polinomNesortat.get(i).getPutere()) {
					double coeficient = polinomSortat.get(z).getCoef() + polinomNesortat.get(i).getCoef();
					polinomSortat.get(z).setCoef(coeficient);
					polinomNesortat.remove(i);
					marime = marime - 1;
					i--;
					break;
				}
			}
			while (!polinomSortat.contains(polinomNesortat.get(i))) {
				polinomSortat.add(polinomNesortat.get(i));
			}
		}
		Operatii.scoateCoeficientiZero(polinomSortat);
		polinomSortat = Operatii.sortarePuteri(polinomSortat);
		return polinomSortat;
	}
/*
 * Aranjeaza polinomul in functie de putere,
 *  functia este apelata in metodat principala de sortare pe nume "sortare"
*/
	private static ArrayList<Monom> sortarePuteri(ArrayList<Monom> deSortat) {
		Collections.sort(deSortat, new Comparator<Monom>() {
			@Override
			public int compare(Monom m1, Monom m2) {

				return Float.compare(m2.getPutere(), m1.getPutere());
			}
		});

		return deSortat;
	}
/*
 * Scoate coefiecienti cu valoare '0' din polinom,
 *  metoda este apelata de funcita principala sortare
*/	
	private static void scoateCoeficientiZero(ArrayList<Monom> polinomDeCuratat) {
		int marime = polinomDeCuratat.size();
		for (int i = 0; i < marime; i++) {
			if (polinomDeCuratat.get(i).getCoef() == 0) {
				polinomDeCuratat.remove(i);
				marime = marime - 1;
				i--;
				break;
			}
		}
	}

	/*
	 *  Aduna la polinomul a pe polinomul b
	 */
	public static Polinom adunare(Polinom a, Polinom b) {
		Polinom buffer = new Polinom();
		buffer.setPolinom(a.getPolinom());
		for (int i = 0; i < b.marime(); i++) {
			buffer.addMonom((b.getMonom(i)));
		}
		buffer.setPolinom(Operatii.sortare(buffer.getPolinom()));
		return buffer;
	}

	/*
	 *  Adauga la polinomul "a", polinomul "b"
	 */
	public static Polinom concatenarePolinoame(Polinom a, Polinom b) {
		Polinom c = new Polinom();
		for (int i = 0; i < b.marime(); i++) {
			a.addMonom((b.getMonom(i)));
		}
		c.setPolinom(Operatii.sortare(c.getPolinom()));
		return c;
	}

	/*
	 *  Scade din polinomul "a" polinomul "b"
	 */
	public static Polinom scadere(Polinom a, Polinom b) {
		Polinom bufferA = new Polinom();
		Polinom bufferB = new Polinom();
		bufferA = a;
		bufferB = b;
		for (Monom m : bufferB.getPolinom()) {
			m.setCoef(-m.getCoef());
			bufferA.addMonom(m);
		}
		bufferA.setPolinom(Operatii.sortare(bufferA.getPolinom()));
		return bufferA;
	}

	/*
	 *  Inmulteste polinomul 'a' cu polinomul 'b'
	 */
	public static Polinom inmultire(Polinom a, Polinom b) {
		Polinom c = new Polinom();
		for (Monom monomA : a.getPolinom()) {
			for (Monom monomB : b.getPolinom()) {
				Monom buf = new Monom(0, 0);
				buf.setCoef(monomA.getCoef() * monomB.getCoef());
				buf.setPutere(monomA.getPutere() + monomB.getPutere());
				c.addMonom(buf);
			}
		}
		c.setPolinom(Operatii.sortare(c.getPolinom()));
		return c;
	}

/* Imparte polinomul deImpartit la Impartitor, se va efectua doar daca
 *gradul celui de al doilea polinom este mai mic decat gradul primului
 *Metoda tine rezultatul ca Coeficient SI Rest intr-o Lista de Polinoame ,	 
dar returneaza un string care contine ambele rezultate
*/
	public static String impartire(Polinom deImpartit, Polinom impartitor) {
		ArrayList<Polinom> rezultatImpartire = new ArrayList<Polinom>();
		deImpartit.setPolinom(Operatii.sortare(deImpartit.getPolinom()));
		impartitor.setPolinom(Operatii.sortare(impartitor.getPolinom()));
		Polinom coeficient = new Polinom();
		Polinom v = new Polinom();
		Polinom rest = new Polinom();
		rest = deImpartit;
		int grad = rest.grad();
		int gradImpartitor = impartitor.grad();
		String rezultatImpartireString = new String();
		if (deImpartit.grad() >= gradImpartitor) {
			while (grad >= gradImpartitor) {
				Monom buff = new Monom();
				buff.setCoef(rest.getMonom(0).getCoef() / impartitor.getMonom(0).getCoef());
				buff.setPutere(rest.getMonom(0).getPutere() - impartitor.getMonom(0).getPutere());
				coeficient.addMonom(buff);
				v = Operatii.inmultirePolinomCuMonom(impartitor, buff);
				rest = Operatii.scadere(deImpartit, v);
				rest.setPolinom(Operatii.sortare(rest.getPolinom()));
				grad = rest.grad();
			}
			rezultatImpartire.add(coeficient);
			rezultatImpartire.add(rest);		
			rezultatImpartireString = " Coeficient : " + coeficient.stringPolinom() + " Rest: " + rest.stringPolinom();
		} else {rezultatImpartireString = "Nu poti imparti un polinom cu grad mai mic decat impartitorul";
		}		
		return rezultatImpartireString;
	}
	
/*
 *  Functie Impartire
*/
	public static Polinom derivare(Polinom a) {
		for (Monom monom : a.getPolinom()) {
			if (monom.getPutere() == 0) {
				monom.setCoef(0);
			}
			monom.setCoef(monom.getCoef() * monom.getPutere());
			monom.setPutere(monom.getPutere() - 1);
		}
		a.setPolinom(Operatii.sortare(a.getPolinom()));
		return a;
	}

	/*
	 *  Functie Integrare	
	*/
	public static Polinom integrare(Polinom a) {
		for (Monom q : a.getPolinom()) {
			q.setCoef(q.getCoef() * (1.0 / (q.getPutere() + 1)));
			q.setPutere(q.getPutere() + 1);
		}
		a.setPolinom(Operatii.sortare(a.getPolinom()));
		return a;
	}
/*
 *Functia Inmultire polinom cu un Monom
 *inmulteste un intreg polinom cu un monom
 *
 */
	public static Polinom inmultirePolinomCuMonom(Polinom polinom, Monom monom) {

		Polinom polinomIntermediar = new Polinom();

		for (Monom monomPolinom : polinom.getPolinom()) {
			Monom buf = new Monom(0, 0);

			buf.setCoef(monomPolinom.getCoef() * monom.getCoef());
			buf.setPutere(monomPolinom.getPutere() + monom.getPutere());
			polinomIntermediar.addMonom(buf);
		}
		polinomIntermediar.setPolinom(Operatii.sortare(polinomIntermediar.getPolinom()));
		return polinomIntermediar;
	}
	
	/*
	 * Regex Polinoame
	 * Transforma un String intr-un obiect de clasa Polinom, care contine o Lista cu toti termeni unui polinom,care se afla intrun string trimis funcitiei
	 * Exemplu : daca stringul trimise este "3x^2+ 5" functia ca recunaoste folosit matcherul , coeficientul '3' ,cu puterea '2' si termenul liber '5'
	 */	 
	public static Polinom regexPolinoame(String stringCeContinePolinom) {
		Polinom polinomRegex = new Polinom();
		String input = stringCeContinePolinom;
		Pattern p = Pattern.compile("(-?\\b\\d+)x\\^(-?\\d+\\b)|(-?\\d+)");
		Matcher m = p.matcher(input);
		while (m.find()) {
			Monom monom = new Monom();
			if (null == m.group(3)) {
				monom.setCoef(Float.parseFloat(m.group(1)));
				monom.setPutere(Integer.parseInt(m.group(2)));				
				polinomRegex.addMonom(monom);		
			} else {
				monom.setCoef(Integer.parseInt(m.group(3)));
				monom.setPutere(0);
				polinomRegex.addMonom(monom);
			}
		}
		return polinomRegex;
	}
}