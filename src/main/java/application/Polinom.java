package application;

import java.util.*;

/*
 * Clasa ce retine toate monomamele intr-un ArraList
 */
public class Polinom {
	// lista ce contine monoamele
	private ArrayList<Monom> polinom = new ArrayList<Monom>();

	public Polinom(int... n) {
		for (int i = 0; i < n.length; i++) {
			if (i % 2 == 0) {
				Monom monom = new Monom(0, 0);
				monom.setCoef(n[i]);
				polinom.add(i / 2, monom);
			}
			if (i % 2 == 1) {
				polinom.get(i / 2).setPutere(n[i]);
			}
		}
	}

	/*
	 * Returneaza marimea unui polinom(numarul de elemente a listei) ex:
	 * "2x^2+3x^1" va returna '2'
	 */
	public int marime() {
		int g = 0;
		for (int i = 0; i < polinom.size(); i++)
			if (polinom.get(i).getCoef() != 0)
				g = i + 1;
		return g;
	}

	/*
	 * Returneaza cea mai mare putere ce o contine un polinom , aceasta
	 * reprezentand gradul unui polinom
	 */
	public int grad() {
		int g = 0;
		for (Monom monom : polinom)
			if (Math.abs(monom.getPutere()) > g)
				g = Math.abs(monom.getPutere());
		return g;
	}

	/*
	 * Returneaza lista ce contine monoamele
	 */
	public ArrayList<Monom> getPolinom() {
		return this.polinom;
	}

	/*
	 * Seteaza lista polinom
	 */
	public void setPolinom(ArrayList<Monom> lista) {
		this.polinom = lista;
	}

	/*
	 * Returneaza un monom ;a indexul dat
	 */
	public Monom getMonom(int index) {
		return polinom.get(index);

	}

	/*
	 * Adauga un monom in lista
	 */
	public void addMonom(Monom monom) {
		this.polinom.add(monom);
	}

	/*
	 * Schimba monomul din Lista polinom , la indexul dat
	 */
	public void setMonom(Monom monom, int index) {
		this.polinom.set(index, monom);
	}
/*
 * Functia genereaza un string bazat de polinom
 */
	public String stringPolinom() {
		String stringPolinom = new String();
		for (int i = 0; i < polinom.size(); i++) {
			Monom monom = polinom.get(i);
			if (polinom.get(i).getPutere() != 0) {
				if (i == 0) {
					stringPolinom = monom.coefString() + "x^" + monom.putereString();
				} else {
					if (polinom.get(i).getCoef() < 0)
						stringPolinom += monom.coefString() + "x^" + monom.putereString();
					else {
						stringPolinom += "+" + monom.coefString() + "x^" + monom.putereString();
					}
				}
			} else {
				if (monom.getCoef() > 0) {
					stringPolinom += "+" + monom.coefString();
				} else {
					stringPolinom += monom.coefString();
				}
			}
		}
		return stringPolinom;
	}
}