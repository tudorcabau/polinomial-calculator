package application;

import org.junit.Test;
import org.junit.*;
import static org.junit.Assert.*;

import javax.swing.plaf.OptionPaneUI;
public class OperatiiTest {
	
	@Test
	public void testPolinomToString() throws Exception {
		String stringPolinom = new String();
		stringPolinom = "2x^4-1x^3+5x^1+5";
		Polinom deComparat = new Polinom(2,3,2,4,-3,3,5,1,5,0);
		deComparat.setPolinom(Operatii.sortare(deComparat.getPolinom()));
		String rezultat =(deComparat.stringPolinom());
		System.out.println(rezultat);
		assertEquals(stringPolinom, rezultat);
	}
	
	@Test
	public void testSortare() throws Exception {
		Polinom polinomNesortat = new Polinom(2,3,2,4,-3,3,5,0);		
		polinomNesortat.setPolinom(Operatii.sortare(polinomNesortat.getPolinom()));
		String stringSortat = new String("2x^4-1x^3+5");
		String stringNesortat = new String(polinomNesortat.stringPolinom());
		assertEquals(stringSortat, stringNesortat);
	}


	@Test
	public void testRegexPolinoame() throws Exception {
		Polinom rezultat = new Polinom();
		String stringPolinom = new String();
		String stringRezultat = new String();
		stringPolinom = "2x^4-1x^3+5";
		rezultat.setPolinom(Operatii.regexPolinoame(stringPolinom).getPolinom());
		stringRezultat = rezultat.stringPolinom();
		assertEquals("2x^4-1x^3+5", stringRezultat);
		
	}
	
	@Test
	public void testAdunare() throws Exception {
		String stringRezultat = new String();
		Polinom primulPolinom= new Polinom(2,3,2,4,-3,3,5,0);
		Polinom alDoileaPolinom = new Polinom(2,4,-2,3,3,0);
		Polinom rezultatPolinom = new Polinom();
		rezultatPolinom.setPolinom(Operatii.adunare(primulPolinom, alDoileaPolinom).getPolinom());
		stringRezultat = rezultatPolinom.stringPolinom();
		assertEquals("4x^4-3x^3+8", stringRezultat);

	}

	@Test
	public void testScadere() throws Exception {
		String stringRezultat = new String();
		Polinom primulPolinom= new Polinom(2,3,2,4,-3,3,5,0);
		Polinom alDoileaPolinom = new Polinom(2,4,-2,3,3,0);
		Polinom rezultatPolinom = new Polinom();
		rezultatPolinom.setPolinom(Operatii.scadere(primulPolinom, alDoileaPolinom).getPolinom());
		stringRezultat = rezultatPolinom.stringPolinom();
		assertEquals("1x^3+2", stringRezultat);

	}

	@Test
	public void testInmultire() throws Exception {
		String stringRezultat = new String();
		Polinom primulPolinom= new Polinom(2,3,2,4,-3,3,5,0);
		Polinom alDoileaPolinom = new Polinom(2,4,-2,3,3,0);
		Polinom rezultatPolinom = new Polinom();
		rezultatPolinom.setPolinom(Operatii.inmultire(primulPolinom, alDoileaPolinom).getPolinom());
		stringRezultat = rezultatPolinom.stringPolinom();
		assertEquals("4x^8-6x^7+2x^6+16x^4-13x^3+15", stringRezultat);

	}

	@Test
	public void testImpartire() throws Exception {		
		Polinom primulPolinom= new Polinom(2,3,2,4,-3,3,5,0);
		Polinom alDoileaPolinom = new Polinom(2,4,-2,3,3,0);	
		assertEquals(" Coeficient : +1 Rest: 1x^3+2", Operatii.impartire(primulPolinom, alDoileaPolinom));
	}

	@Test
	public void testDerivare() throws Exception {
		String stringRezultat = new String();
		Polinom primulPolinom= new Polinom(2,3,2,4,-3,3,5,0);
		Polinom rezultatPolinom = new Polinom();
		rezultatPolinom.setPolinom(Operatii.derivare(primulPolinom).getPolinom());
		stringRezultat = rezultatPolinom.stringPolinom();
		assertEquals("8x^3-3x^2", stringRezultat);

	}

	@Test
	public void testIntegrare() throws Exception {
		String stringRezultat = new String();
		Polinom primulPolinom= new Polinom(2,3,2,4,-3,3,5,0);
		Polinom rezultatPolinom = new Polinom();
		rezultatPolinom.setPolinom(Operatii.integrare(primulPolinom).getPolinom());
		stringRezultat = rezultatPolinom.stringPolinom();
		assertEquals("0.4x^5-0.25x^4+5x^1", stringRezultat);

	}	
}
